<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191026073443 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE landing (id INT AUTO_INCREMENT NOT NULL, config_id INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, template_name VARCHAR(255) NOT NULL, data JSON NOT NULL COMMENT \'(DC2Type:json_array)\', UNIQUE INDEX UNIQ_EF3ACE1524DB0683 (config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE landing_config (id INT AUTO_INCREMENT NOT NULL, is_available_for_user TINYINT(1) NOT NULL, is_available_shops TINYINT(1) NOT NULL, is_available_statistics TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE landing ADD CONSTRAINT FK_EF3ACE1524DB0683 FOREIGN KEY (config_id) REFERENCES landing_config (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE landing DROP FOREIGN KEY FK_EF3ACE1524DB0683');
        $this->addSql('DROP TABLE landing');
        $this->addSql('DROP TABLE landing_config');
    }
}
