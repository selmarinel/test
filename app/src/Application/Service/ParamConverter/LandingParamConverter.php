<?php

declare(strict_types=1);

namespace App\Application\Service\ParamConverter;

use App\Infrastructure\Landing\DataTransferObject\LandingDTO;
use App\Infrastructure\Landing\Repository\LandingRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class LandingParamConverter.
 */
class LandingParamConverter implements ParamConverterInterface
{
    /**
     * @var LandingRepository
     */
    private $landingRepository;

    /**
     * LandingParamConverter constructor.
     * @param LandingRepository $landingRepository
     */
    public function __construct(LandingRepository $landingRepository)
    {
        $this->landingRepository = $landingRepository;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     *
     * @return void
     */
    public function apply(Request $request, ParamConverter $configuration): void
    {
        $options = $configuration->getOptions();

        $slug = isset($options['slug']) ? $options['slug'] : 'slug';

        if (!$landing = $this->landingRepository->findBySlug($request->attributes->get($slug))) {
            throw new NotFoundHttpException();
        }
        //todo as Factory
        $landingDTOClass = $configuration->getClass();

        /** @var LandingDTO $landingDTO */
        $landingDTO = new $landingDTOClass();
        $landingDTO->setSlug($landing->getSlug());
        $landingDTO->setData($landing->getData());
        $landingDTO->setTemplateName($landing->getTemplateName());

        $landingDTO->setIsAvailableForUser($landing->getConfig()->isAvailableForUser());
        $landingDTO->setIsAvailableShops($landing->getConfig()->isAvailableShops());
        $landingDTO->setIsAvailableStatistic($landing->getConfig()->isAvailableStatistics());
//<--
        $request->attributes->set($configuration->getName(), $landingDTO);
    }

    /**
     * Checks if the object is supported.
     *
     * @param ParamConverter $configuration
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        if ($configuration->getClass() === LandingDTO::class) {
            return true;
        }

        return false;
    }
}
