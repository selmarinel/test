<?php

namespace App\Application\DataFixtures;

use App\Infrastructure\Landing\Entity\Landing;
use App\Infrastructure\Landing\Entity\LandingConfig;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures.
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $landing = new Landing();

            $landing->setTemplateName('t' . $i)
                ->setData(['f' => 'r'])
                ->setSlug('l' . $i);

            $landingConfig = new LandingConfig();
            $landingConfig->setIsAvailableStatistics(true);
            $landingConfig->setIsAvailableShops(true);
            $landingConfig->setIsAvailableForUser(true);

            $landing->setConfig($landingConfig);

            $manager->persist($landing);
        }

        $manager->flush();
    }
}
