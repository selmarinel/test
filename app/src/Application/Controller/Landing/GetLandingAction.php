<?php

declare(strict_types=1);

namespace App\Application\Controller\Landing;

use App\Application\Security\LandingVoter;
use App\Domain\Landing\DataTransferObject\LandingInterface;
use App\Domain\Landing\Service\LandingHandlerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Throwable;
use Twig\Environment;
use App\Infrastructure\Landing\DataTransferObject\LandingDTO;

/**
 * Class GetLandingAction.
 * @Route(path="/{slug}", methods={"GET"}, name="get_landing")
 */
class GetLandingAction
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var LandingHandlerInterface
     */
    private $handler;

    /**
     * GetLandingAction constructor.
     *
     * @param Security $security
     * @param Environment $environment
     * @param LandingHandlerInterface $handler
     */
    public function __construct(Security $security, Environment $environment, LandingHandlerInterface $handler)
    {
        $this->security = $security;
        $this->twig = $environment;
        $this->handler = $handler;
    }

    /**
     * @ParamConverter(name="landing", converter="landing_converter", class=LandingDTO::class, )
     *
     * @param LandingInterface $landing
     *
     * @return Response
     *
     * @throws Throwable
     */
    public function __invoke(LandingInterface $landing)
    {
        if (!$this->security->isGranted(LandingVoter::IS_AVAILABLE_FOR_AUTH_USER, $landing)) {
            throw new NotFoundHttpException;
        }

        $this->handler->handle($landing);

        return new Response($this->twig->render($landing->getTemplateName() . '.html.twig', $landing->getData()));
    }
}
