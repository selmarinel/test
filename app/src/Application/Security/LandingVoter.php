<?php

declare(strict_types=1);

namespace App\Application\Security;

use App\Domain\Landing\DataTransferObject\LandingInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class LandingVoter.
 */
class LandingVoter extends Voter
{
    public const IS_AVAILABLE_FOR_AUTH_USER = 'IS_AVAILABLE_LANDING';

    /**
     * @param string $attribute
     * @param mixed $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        if (in_array($attribute, [self::IS_AVAILABLE_FOR_AUTH_USER]) && $subject instanceof LandingInterface) {
            return true;
        }

        return false;
    }

    /**
     * @param string $attribute
     * @param LandingInterface $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::IS_AVAILABLE_FOR_AUTH_USER:
                if ($user = $this->getUser($token)) {
                    return $subject->isAvailableForUser();
                }

                return true;
        }

        return false;
    }

    /**
     * todo
     * @param TokenInterface $token
     *
     * @return UserInterface|null
     */
    public function getUser(TokenInterface $token): ?UserInterface
    {
        return null;
    }
}
