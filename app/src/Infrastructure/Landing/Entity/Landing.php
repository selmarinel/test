<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Landing\Repository\LandingRepository;

/**
 * Class Landing.
 *
 * @ORM\Entity(repositoryClass=LandingRepository::class)
 */
class Landing
{

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $templateName;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $data;

    /**
     * @var LandingConfig
     *
     * @ORM\OneToOne(targetEntity=LandingConfig::class, cascade={"persist"})
     * @ORM\JoinColumn(name="config_id", referencedColumnName="id")
     */
    private $config;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * @param string $templateName
     *
     * @return self
     */
    public function setTemplateName(string $templateName): self
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return LandingConfig
     */
    public function getConfig(): LandingConfig
    {
        return $this->config;
    }

    /**
     * @param LandingConfig $config
     *
     * @return self
     */
    public function setConfig(LandingConfig $config): self
    {
        $this->config = $config;

        return $this;
    }

}
