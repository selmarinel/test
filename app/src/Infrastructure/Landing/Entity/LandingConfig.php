<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LandingConfig.
 *
 * @ORM\Entity()
 */
class LandingConfig
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isAvailableForUser;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isAvailableShops;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isAvailableStatistics;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableForUser(): bool
    {
        return $this->isAvailableForUser;
    }

    /**
     * @param bool $isAvailableForUser
     *
     * @return self
     */
    public function setIsAvailableForUser(bool $isAvailableForUser): self
    {
        $this->isAvailableForUser = $isAvailableForUser;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableShops(): bool
    {
        return $this->isAvailableShops;
    }

    /**
     * @param bool $isAvailableShops
     *
     * @return self
     */
    public function setIsAvailableShops(bool $isAvailableShops): self
    {
        $this->isAvailableShops = $isAvailableShops;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableStatistics(): bool
    {
        return $this->isAvailableStatistics;
    }

    /**
     * @param bool $isAvailableStatistics
     *
     * @return self
     */
    public function setIsAvailableStatistics(bool $isAvailableStatistics): self
    {
        $this->isAvailableStatistics = $isAvailableStatistics;

        return $this;
    }
}
