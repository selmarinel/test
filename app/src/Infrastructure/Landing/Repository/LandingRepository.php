<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Repository;

use App\Infrastructure\Landing\Entity\Landing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class LandingRepository.
 */
class LandingRepository extends ServiceEntityRepository
{
    /**
     * LandingRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Landing::class);
    }

    /**
     * @param string $slug
     *
     * @return Landing|null
     */
    public function findBySlug(string $slug): ?Landing
    {
        /** @var Landing $landing */
        $landing = $this->findOneBy(['slug' => $slug]);

        return $landing;
    }
}
