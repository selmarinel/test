<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Service\Mediator;

use App\Domain\Landing\DataTransferObject\LandingInterface;
use App\Domain\Landing\Service\ConfigurationMediatorInterface;

/**
 * Class TemplateStatisticMediator.
 */
class TemplateStatisticMediator implements ConfigurationMediatorInterface
{
    /**
     * @param LandingInterface $landing
     *
     * @return ConfigurationMediatorInterface
     */
    public function handle(LandingInterface $landing): ConfigurationMediatorInterface
    {
        if ($landing->isAvailableStatistic()) {
            $landing->setData(array_merge($landing->getData(), ['statistic' => 'statisticData']));
        }

        return $this;
    }
}
