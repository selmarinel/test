<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Service\Mediator;

use App\Domain\Landing\DataTransferObject\LandingInterface;
use App\Domain\Landing\Service\ConfigurationMediatorInterface;

/**
 * Class SetTemplateDataChain
 */
class SetTemplateDataMediator implements ConfigurationMediatorInterface
{
    private const DEFAULT__DATA = [
        'noFollowRobots' => true,
        'noFollowYandex' => true,
    ];

    public function handle(LandingInterface $landing): ConfigurationMediatorInterface
    {
        $data = $landing->getData();
        $landing->setData(array_merge($data, self::DEFAULT__DATA));

        return $this;
    }
}
