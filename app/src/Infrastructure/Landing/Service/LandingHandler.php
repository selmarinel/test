<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\Service;

use App\Domain\Landing\DataTransferObject\LandingInterface;
use App\Domain\Landing\Service\ConfigurationMediatorInterface;
use App\Domain\Landing\Service\LandingHandlerInterface;

/**
 * Class LandingHandler.
 */
class LandingHandler implements LandingHandlerInterface
{
    /**
     * @var ConfigurationMediatorInterface[]
     */
    private $mediators = [];

    /**
     * LandingHandler constructor.
     *
     * @param iterable $mediators
     */
    public function __construct(iterable $mediators)
    {
        foreach ($mediators as $mediator) {
            if ($mediator instanceof ConfigurationMediatorInterface) {
                $this->mediators[] = $mediator;
            }
        }
    }

    /**
     * @param LandingInterface $landing
     *
     * @return LandingInterface
     */
    public function handle(LandingInterface $landing): LandingInterface
    {
        foreach ($this->mediators as $configurationMediator) {
            $configurationMediator->handle($landing);
        }

        return $landing;
    }
}
