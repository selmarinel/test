<?php

declare(strict_types=1);

namespace App\Infrastructure\Landing\DataTransferObject;

use App\Domain\Landing\DataTransferObject\LandingInterface;

/**
 * Class LandingDTO.
 */
class LandingDTO implements LandingInterface
{
    /**
     * @var string
     */
    private $slug = '';

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var bool
     */
    private $isAvailableForUser = true;

    /**
     * @var bool
     */
    private $isAvailableShops = true;

    /**
     * @var bool
     */
    private $isAvailableStatistics = true;

    /**
     * @var string
     */
    private $templateName = '';

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data): LandingInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableForUser(): bool
    {
        return $this->isAvailableForUser;
    }

    /**
     * @param bool $isAvailableForUser
     *
     * @return self
     */
    public function setIsAvailableForUser(bool $isAvailableForUser): self
    {
        $this->isAvailableForUser = $isAvailableForUser;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableShops(): bool
    {
        return $this->isAvailableShops;
    }

    /**
     * @param bool $isAvailableShops
     *
     * @return self
     */
    public function setIsAvailableShops(bool $isAvailableShops): self
    {
        $this->isAvailableShops = $isAvailableShops;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailableStatistic(): bool
    {
        return $this->isAvailableStatistics;
    }

    /**
     * @param bool $isAvailableStatistics
     *
     * @return self
     */
    public function setIsAvailableStatistic(bool $isAvailableStatistics): self
    {
        $this->isAvailableStatistics = $isAvailableStatistics;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * @param string $templateName
     *
     * @return self
     */
    public function setTemplateName(string $templateName): self
    {
        $this->templateName = $templateName;

        return $this;
    }
}
