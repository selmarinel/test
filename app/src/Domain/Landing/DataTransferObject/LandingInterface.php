<?php

declare(strict_types=1);

namespace App\Domain\Landing\DataTransferObject;

/**
 * Interface LandingInterface.
 */
interface LandingInterface
{
    /**
     * @return bool
     */
    public function isAvailableForUser(): bool;

    /**
     * @return bool
     */
    public function isAvailableShops(): bool;

    /**
     * @return bool
     */
    public function isAvailableStatistic(): bool;

    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @return string
     */
    public function getTemplateName(): string;

    ///
    ///
    public function setData(array $data): LandingInterface;
}
