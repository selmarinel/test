<?php

namespace App\Domain\Landing\Service;

use App\Domain\Landing\DataTransferObject\LandingInterface;

/**
 * Interface LandingHandlerInterface.
 */
interface LandingHandlerInterface
{
    /**
     * @param LandingInterface $landing
     *
     * @return LandingInterface
     */
    public function handle(LandingInterface $landing): LandingInterface;
}