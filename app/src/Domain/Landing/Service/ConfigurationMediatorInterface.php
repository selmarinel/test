<?php

namespace App\Domain\Landing\Service;


use App\Domain\Landing\DataTransferObject\LandingInterface;

/**
 * Interface ConfigurationMediatorInterface
 */
interface ConfigurationMediatorInterface
{
    /**
     * @param LandingInterface $landing
     *
     * @return ConfigurationMediatorInterface
     */
    public function handle(LandingInterface $landing): ConfigurationMediatorInterface;
}
